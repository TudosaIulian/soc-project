﻿namespace Asabmblor_UC16_T_Iulian
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.infoProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.infoDeveloperToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contactToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lblOpenFile = new System.Windows.Forms.Label();
            this.lblSaveFile = new System.Windows.Forms.Label();
            this.txtOpenFile = new System.Windows.Forms.TextBox();
            this.txtSaveFile = new System.Windows.Forms.TextBox();
            this.txtintrare = new System.Windows.Forms.TextBox();
            this.txtiesire = new System.Windows.Forms.TextBox();
            this.btnmagic = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.infoProjectToolStripMenuItem,
            this.infoDeveloperToolStripMenuItem,
            this.contactToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1549, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // infoProjectToolStripMenuItem
            // 
            this.infoProjectToolStripMenuItem.Name = "infoProjectToolStripMenuItem";
            this.infoProjectToolStripMenuItem.Size = new System.Drawing.Size(80, 20);
            this.infoProjectToolStripMenuItem.Text = "Info Project";
            this.infoProjectToolStripMenuItem.Click += new System.EventHandler(this.infoProjectToolStripMenuItem_Click);
            // 
            // infoDeveloperToolStripMenuItem
            // 
            this.infoDeveloperToolStripMenuItem.Name = "infoDeveloperToolStripMenuItem";
            this.infoDeveloperToolStripMenuItem.Size = new System.Drawing.Size(96, 20);
            this.infoDeveloperToolStripMenuItem.Text = "Info Developer";
            this.infoDeveloperToolStripMenuItem.Click += new System.EventHandler(this.infoDeveloperToolStripMenuItem_Click);
            // 
            // contactToolStripMenuItem
            // 
            this.contactToolStripMenuItem.Name = "contactToolStripMenuItem";
            this.contactToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.contactToolStripMenuItem.Text = "Contact ";
            this.contactToolStripMenuItem.Click += new System.EventHandler(this.contactToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // lblOpenFile
            // 
            this.lblOpenFile.AutoSize = true;
            this.lblOpenFile.Font = new System.Drawing.Font("Monotype Corsiva", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblOpenFile.ForeColor = System.Drawing.Color.Red;
            this.lblOpenFile.Location = new System.Drawing.Point(165, 66);
            this.lblOpenFile.Name = "lblOpenFile";
            this.lblOpenFile.Size = new System.Drawing.Size(157, 45);
            this.lblOpenFile.TabIndex = 2;
            this.lblOpenFile.Text = "Open File";
            // 
            // lblSaveFile
            // 
            this.lblSaveFile.AutoSize = true;
            this.lblSaveFile.Font = new System.Drawing.Font("Monotype Corsiva", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblSaveFile.ForeColor = System.Drawing.Color.Red;
            this.lblSaveFile.Location = new System.Drawing.Point(1084, 66);
            this.lblSaveFile.Name = "lblSaveFile";
            this.lblSaveFile.Size = new System.Drawing.Size(151, 45);
            this.lblSaveFile.TabIndex = 3;
            this.lblSaveFile.Text = "Save File";
            // 
            // txtOpenFile
            // 
            this.txtOpenFile.Location = new System.Drawing.Point(173, 116);
            this.txtOpenFile.Name = "txtOpenFile";
            this.txtOpenFile.Size = new System.Drawing.Size(149, 20);
            this.txtOpenFile.TabIndex = 4;
            // 
            // txtSaveFile
            // 
            this.txtSaveFile.Location = new System.Drawing.Point(1092, 116);
            this.txtSaveFile.Name = "txtSaveFile";
            this.txtSaveFile.Size = new System.Drawing.Size(143, 20);
            this.txtSaveFile.TabIndex = 5;
            // 
            // txtintrare
            // 
            this.txtintrare.Location = new System.Drawing.Point(104, 156);
            this.txtintrare.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtintrare.Multiline = true;
            this.txtintrare.Name = "txtintrare";
            this.txtintrare.ReadOnly = true;
            this.txtintrare.Size = new System.Drawing.Size(288, 310);
            this.txtintrare.TabIndex = 7;
            // 
            // txtiesire
            // 
            this.txtiesire.Location = new System.Drawing.Point(1021, 156);
            this.txtiesire.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtiesire.Multiline = true;
            this.txtiesire.Name = "txtiesire";
            this.txtiesire.ReadOnly = true;
            this.txtiesire.Size = new System.Drawing.Size(288, 310);
            this.txtiesire.TabIndex = 8;
            // 
            // btnmagic
            // 
            this.btnmagic.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.btnmagic.BackgroundImage = global::Asabmblor_UC16_T_Iulian.Properties.Resources._9UdCkO;
            this.btnmagic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnmagic.Font = new System.Drawing.Font("Georgia", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnmagic.ForeColor = System.Drawing.Color.DarkRed;
            this.btnmagic.Location = new System.Drawing.Point(619, 266);
            this.btnmagic.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnmagic.Name = "btnmagic";
            this.btnmagic.Size = new System.Drawing.Size(229, 95);
            this.btnmagic.TabIndex = 9;
            this.btnmagic.Text = "The Magic Button";
            this.btnmagic.UseVisualStyleBackColor = true;
            this.btnmagic.Click += new System.EventHandler(this.btnmagic_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Image = global::Asabmblor_UC16_T_Iulian.Properties.Resources.logo221;
            this.pictureBox1.Location = new System.Drawing.Point(1448, 496);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(101, 118);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1549, 615);
            this.Controls.Add(this.btnmagic);
            this.Controls.Add(this.txtiesire);
            this.Controls.Add(this.txtintrare);
            this.Controls.Add(this.txtSaveFile);
            this.Controls.Add(this.txtOpenFile);
            this.Controls.Add(this.lblSaveFile);
            this.Controls.Add(this.lblOpenFile);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "UC16 Project";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem infoProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem infoDeveloperToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contactToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblOpenFile;
        private System.Windows.Forms.Label lblSaveFile;
        private System.Windows.Forms.TextBox txtOpenFile;
        private System.Windows.Forms.TextBox txtSaveFile;
        private System.Windows.Forms.TextBox txtintrare;
        private System.Windows.Forms.TextBox txtiesire;
        private System.Windows.Forms.Button btnmagic;
    }
}

