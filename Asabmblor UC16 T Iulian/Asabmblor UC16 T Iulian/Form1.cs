﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;


namespace Asabmblor_UC16_T_Iulian
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            Thread t = new Thread(new ThreadStart(StartForm));
            t.Start();
            Thread.Sleep(5000);
            InitializeComponent();
            t.Abort();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        public void StartForm()
        { Application.Run(new frmSplash()); }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void contactToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Email: iulian.tudosa@student.usv.ro \n Site: www.stud.usv.ro/~iutudosa");
        }

        private void infoDeveloperToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This project was created by Tudosa Iulian , Student at University Stefan cel Mare Suceava");
        }

        private void infoProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This project was created in Visual Studio 2015 with C# language. \n The internal arhitecture of the processor UC 16 consists mainly of a collection of registry connected by a network of highways. ");

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void btnmagic_Click(object sender, EventArgs e)
        {
            TextProcessor t = new TextProcessor();
            string s;
            if (txtOpenFile.Text == "") MessageBox.Show("Numele fisierului sursa nu a fost introdus");
            else if (txtSaveFile.Text == "") MessageBox.Show("Numele fisierului destinatie nu a fost introdus");
            else
            {
                t.getFileName(txtOpenFile.Text);
                if (t.openFile())
                {
                    txtintrare.Text = "";
                    txtiesire.Text = "";
                    int k = 0;
                    s = t.readLine();
                    string s2;
                    do
                    {

                        k++;
                        txtintrare.Text += s + System.Environment.NewLine;
                        s2 = t.stringConvert(s);
                        if (s2 != "Eroare la linia ")
                        {
                            txtiesire.Text += s2 + ',' + System.Environment.NewLine;
                        }
                        else txtiesire.Text += s2 + k;
                        s = t.readLine();
                    }
                    while (s != null);
                    t.closeFile();
                    t.openWriteFile(txtSaveFile.Text);
                    t.writeToFile(txtiesire.Text);
                    t.closeWriteFile();
                }
            }
        }
    }
}
