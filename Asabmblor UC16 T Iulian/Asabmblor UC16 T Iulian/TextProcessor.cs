﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asabmblor_UC16_T_Iulian
{
    class TextProcessor
    {
        string filename;
        TextReader tRead;
        TextWriter tWrite;
        public TextProcessor()//constructor
        {
            filename = "";
        }
        public void getFileName(string name)
        {
            filename = name;
        }
        public bool openFile()
        {
            try
            {
                tRead = new StreamReader(filename);
                return true;
            }
            catch (Exception e)
            {
                return false;
                throw (e);
            }
        }
        public bool openWriteFile(string name)
        {
            try
            {
                tWrite = new StreamWriter(name);
                return true;
            }
            catch (Exception e)
            {
                return false;
                throw (e);
            }
        }
        public void writeToFile(string s)
        {
            tWrite.Write(s);
        }
        public void closeWriteFile()
        {
            tWrite.Close();
        }
        public void closeFile()
        {
            tRead.Close();
        }
        public string readLine()
        {
            string s = tRead.ReadLine();
            return s;
        }
        public string stringConvert(string line)
        { 
        string result = "";
            string[] components = new string[4];
            char[] seps = { ' ' };
            string nrstring;
            components = line.Split();
            switch (components[0])
            {
                #region ad
                case "ad": result += '0';
                    if (components[1].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[1][1];
                        result += nrstring;
                    }
                    else
                    {
                        int decvalue = new int();
                        nrstring = "";
                        nrstring += components[1][1];
                        nrstring += components[1][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    if (components[2].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[2][1];
                        result += nrstring;
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[2][1];
                        nrstring += components[2][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    if (components[3].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[3][1];
                        result += nrstring;
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[3][1];
                        nrstring += components[3][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    break;
               
                #endregion
            
             #region sb
                case "sb": result += '1';
                    if (components[1].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[1][1];
                        result += nrstring;
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[1][1];
                        nrstring += components[1][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    if (components[2].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[2][1];
                        result += nrstring;
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[2][1];
                        nrstring += components[2][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    if (components[3].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[3][1];
                        result += nrstring;
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[3][1];
                        nrstring += components[3][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    break;
                #endregion
                #region ml
                case "ml": result += '2';
                    if (components[1].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[1][1];
                        result += nrstring;
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[1][1];
                        nrstring += components[1][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    if (components[2].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[2][1];
                        result += nrstring;
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[2][1];
                        nrstring += components[2][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    if (components[3].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[3][1];
                        result += nrstring;
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[3][1];
                        nrstring += components[3][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    break;
                #endregion
                #region dv
                case "dv": result += '3';
                    if (components[1].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[1][1];
                        result += nrstring;
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[1][1];
                        nrstring += components[1][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    if (components[2].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[2][1];
                        result += nrstring;
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[2][1];
                        nrstring += components[2][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    if (components[3].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[3][1];
                        result += nrstring;
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[3][1];
                        nrstring += components[3][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    break;
                #endregion
                #region adi
                case "adi": result += '4';
                    if (components[1].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[1][1];
                        result += nrstring;
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[1][1];
                        nrstring += components[1][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    if (components[2].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[2][1];
                        result += nrstring;
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[2][1];
                        nrstring += components[2][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    if (components[3][0] != '-')
                    {
                        result += components[3][1];
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[3][1];
                        nrstring += components[3][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    break;
                #endregion

                #region sbi
                case "sbi": result += '5';
                    if (components[1].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[1][1];
                        result += nrstring;
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[1][1];
                        nrstring += components[1][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                    }
                    if (components[2].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[2][1];
                        result += nrstring;
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[2][1];
                        nrstring += components[2][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    if (components[3][0] != '-')
                    {
                        result += components[3][1];
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[3][1];
                        nrstring += components[3][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    break;
                #endregion
                #region rti
                case "rti":
                    result += "6";
                    break;
                #endregion
                #region sh
                case "sh":
                    result += "7";
                    if (components[1].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[1][1];
                        result += nrstring;
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[1][1];
                        nrstring += components[1][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    if (components[2].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[2][1];
                        result += nrstring;
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[2][1];
                        nrstring += components[2][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    if (components[3].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[3][1];
                        result += nrstring;
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[3][1];
                        nrstring += components[3][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    break;
                #endregion
                #region and
                case "and":
                    result += "8";
                    if (components[1].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[1][1];
                        result += nrstring;
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[1][1];
                        nrstring += components[1][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    if (components[2].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[2][1];
                        result += nrstring;
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[2][1];
                        nrstring += components[2][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    if (components[3].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[3][1];
                        result += nrstring;
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[3][1];
                        nrstring += components[3][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    break;
                #endregion
                #region or
                case "or":
                    result += "9";
                    if (components[1].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[1][1];
                        result += nrstring;
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[1][1];
                        nrstring += components[1][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    if (components[2].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[2][1];
                        result += nrstring;
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[2][1];
                        nrstring += components[2][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    if (components[3].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[3][1];
                        result += nrstring;
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[3][1];
                        nrstring += components[3][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    break;
                #endregion
                #region xor
                case "xor":
                    result += "A";
                    if (components[1].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[1][1];
                        result += nrstring;
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[1][1];
                        nrstring += components[1][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    if (components[2].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[2][1];
                        result += nrstring;
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[2][1];
                        nrstring += components[2][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    if (components[3].Length == 2)
                    {
                        nrstring = "";
                        nrstring += components[3][1];
                        result += nrstring;
                    }
                    else
                    {
                        nrstring = "";
                        int decvalue = new int();
                        nrstring += components[3][1];
                        nrstring += components[3][2];
                        int.TryParse(nrstring, out decvalue);
                        nrstring = decvalue.ToString("X");
                        result += nrstring;
                    }
                    break;
                #endregion
             default: result = "Eroare la linia ";
                    break;
        }
            return result;
        
    
        }
    }
}
